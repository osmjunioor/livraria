﻿using System.ComponentModel.DataAnnotations;

namespace Livraria.Application.Models
{
    public class LivroViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Informe o título")]
        [StringLength(50, ErrorMessage = "O título deve conter de {2} a {1} caracteres.", MinimumLength = 5)]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Informe a descrição.")]
        [StringLength(1000, ErrorMessage = "A descrição deve conter de {2} a {1} caracteres.", MinimumLength = 10)]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Informe o ano de lançamento.")]
        [Range(1500, 2018, ErrorMessage = "O Ano deve estar entre {1} e {2}.")]
        public int Ano { get; set; }

        [Required(ErrorMessage = "Informe o valor.")]
        [Range(0.01, 100000, ErrorMessage = "O valor deve estar entre R${1} e R${2}.")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Valor { get; set; }
    }
}
