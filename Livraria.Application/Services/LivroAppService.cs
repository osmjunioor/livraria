﻿using Livraria.Application.Interfaces;
using Livraria.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Livraria.Application.Services
{
    public class LivroAppService : ILivroAppService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly HttpClient _LivrariaAPI;

        public LivroAppService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _LivrariaAPI = _httpClientFactory.CreateClient("LIVRARIA_API");
        }

        public void Delete(int id)
        {
            var response = _LivrariaAPI.DeleteAsync(_LivrariaAPI.BaseAddress + $"api/Livros/" + id).Result;
            response.EnsureSuccessStatusCode();
        }

        public List<Livro> GetAll()
        {
            try
            {
                var response = _LivrariaAPI.GetAsync(_LivrariaAPI.BaseAddress + $"api/Livros").Result;
                response.EnsureSuccessStatusCode();
                var conteudo = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<Livro>>(conteudo);
            }
            catch (Exception)
            {
                return new List<Livro>();
            }

        }

        public Livro GetById(int id)
        {
            try
            {
                var response = _LivrariaAPI.GetAsync(_LivrariaAPI.BaseAddress + $"api/Livros/" + id.ToString()).Result;
                response.EnsureSuccessStatusCode();
                var conteudo = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<Livro>(conteudo);
            }
            catch (Exception)
            {
                return new Livro();
            }
        }

        public Livro Insert(Livro livro)
        {
            try
            {
                if (!livro.IsValid())
                    return livro;
                var json = new StringContent(JsonConvert.SerializeObject(livro), Encoding.UTF8, "application/json");
                var response = _LivrariaAPI.PostAsync(_LivrariaAPI.BaseAddress + $"api/Livros", json).Result;
                response.EnsureSuccessStatusCode();
                var conteudo = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<Livro>(conteudo);
            }
            catch (Exception)
            {
                return livro;
            }

        }

        public Livro Update(Livro livro)
        {
            try
            {
                if (!livro.IsValid())
                    return livro;
                var json = new StringContent(JsonConvert.SerializeObject(livro), Encoding.UTF8, "application/json");
                var response = _LivrariaAPI.PutAsync(_LivrariaAPI.BaseAddress + $"api/Livros/" + livro.Id, json).Result;
                response.EnsureSuccessStatusCode();
                return livro;
            }
            catch (Exception)
            {
                return livro;
            }

        }
    }
}
