﻿using Livraria.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Livraria.Api.Data
{
    public class LivrariaDbContext : IdentityDbContext
    {
        public LivrariaDbContext(DbContextOptions options) : base(options)
        {


        }
        public DbSet<Livro> Livro { get; set; }
    }
}
