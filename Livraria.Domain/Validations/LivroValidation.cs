﻿using Livraria.Domain.Entities;
using System;
using FluentValidation;

namespace Livraria.Domain.Validations
{
    public class LivroValidation : AbstractValidator<Livro>
    {
        public LivroValidation()
        {
            RuleFor(x => x.Titulo).MinimumLength(5).MaximumLength(50).WithMessage("O titulo deve conter entre 5 e 50 caracteres");
            RuleFor(x => x.Descricao).MinimumLength(5).MaximumLength(1000).WithMessage("O descrição deve conter entre 5 e 1000 caracteres");
            RuleFor(x => x.Ano).GreaterThanOrEqualTo(1500).LessThanOrEqualTo(DateTime.Now.Year).WithMessage($"O Ano deve estar entre 1500 e {DateTime.Now.Year.ToString()}.");
            RuleFor(x => x.Valor).GreaterThanOrEqualTo((decimal)0.01).LessThanOrEqualTo(100000).WithMessage("O valor deve estar entre R$0.01 e R$100000.");
        }
    }
}
