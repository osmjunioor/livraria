﻿using Livraria.Domain.Entities;
using System.Collections.Generic;

namespace Livraria.Application.Interfaces
{
    public interface ILivroAppService
    {
        Livro Insert(Livro livro);
        Livro Update(Livro livro);
        void Delete(int id);
        List<Livro> GetAll();
        Livro GetById(int id);
    }
}
