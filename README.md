# Livraria

## Cenário

Uma livraria da cidade teve um aumento no número de seus exemplares e está com um problema para identificar todos os livros que possui em estoque. Para ajudar a livraria foi solicitado a você desenvolver uma aplicação para gerenciar estes exemplares.

## Requisitos

* O sistema deverá mostrar todos os livros cadastrados ordenados de forma ascendente pelo nome

* O sistema deverá permitir criar, editar e excluir um livro.

* Os livros devem ser persistidos em um banco de dados.

* O frontend deve ser escrito emJavascript/html (framework JS fica a critério do desenvolvedor)

* Deve ser utilizado uma interface REST para se comunicar com o frontend (o framework de REST fica a critério do desenvolvedor)

* Para persistência dos dados deve ser utilizado algum framework de ORM, caso o banco escolhido seja NOSql desconsidere este requisito.

## Observações

* O Backend deve ser escrito em .NET

* O Frontend deve ser escrito em HTML/JS

* Usabilidade, UX do frontend não serão critérios avaliado.

* É obrigatório o uso de REST para resolver o problema.

* Testes unitários, divisão das camadas, modelagem são fatores que serão levados em conta.
