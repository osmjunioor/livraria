﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Livraria.Api.Migrations
{
    public partial class ChageTypeValorCollumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Valor",
                table: "Livro",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Valor",
                table: "Livro",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
