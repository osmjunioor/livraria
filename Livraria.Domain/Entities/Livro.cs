﻿using Livraria.Domain.Validations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Livraria.Domain.Entities
{
    public class Livro
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public int Ano { get; set; }
        public decimal Valor { get; set; }

        [NotMapped]
        private LivroValidation _validator;
        public Livro()
        {
            _validator = new LivroValidation();
        }

        public bool IsValid()
        {
            return _validator.Validate(this).IsValid;
        }

        public List<string> MessagesValidations()
        {
            return !IsValid() ? _validator.Validate(this).Errors.Select(a => a.ErrorMessage).ToList() : new List<string>();
        }

        public Livro(int id, string titulo, string descricao, int ano, decimal valor)
        {
            Id = id;
            Titulo = titulo;
            Descricao = descricao;
            Ano = ano;
            Valor = valor;
            _validator = new LivroValidation();
        }
    }
}
