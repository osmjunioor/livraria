﻿using Livraria.Domain.Entities;
using Livraria.Domain.Validations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Livraria.Test
{
    [TestClass]
    public class LivroUnitTest
    {
        [TestMethod]
        public void TesteTituloTrue()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsTrue(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteTituloFalse()
        {
            var livro = new Livro(1, "Test", "Descrição Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsFalse(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteDescricaoTrue()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsTrue(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteDescricaoFalse()
        {
            var livro = new Livro(1, "Titulo Teste", "Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsTrue(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteAnoTrue()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsTrue(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteAnoFalse()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 1499, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsFalse(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteValorTrue()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 2018, 37.50m);
            var livroValidation = new LivroValidation();
            Assert.IsTrue(livroValidation.Validate(livro).IsValid);
        }

        [TestMethod]
        public void TesteValorFalse()
        {
            var livro = new Livro(1, "Titulo Teste", "Descrição Teste", 2018, 0);
            var livroValidation = new LivroValidation();
            Assert.IsFalse(livroValidation.Validate(livro).IsValid);
        }
    }
}
