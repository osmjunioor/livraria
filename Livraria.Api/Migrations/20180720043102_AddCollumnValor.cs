﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Livraria.Api.Migrations
{
    public partial class AddCollumnValor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Valor",
                table: "Livro",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Valor",
                table: "Livro");
        }
    }
}
