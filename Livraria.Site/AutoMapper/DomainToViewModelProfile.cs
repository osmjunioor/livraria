﻿using AutoMapper;
using Livraria.Application.Models;
using Livraria.Domain.Entities;

namespace Livraria.Site.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            CreateMap<Livro, LivroViewModel>();
        }
    }
}
