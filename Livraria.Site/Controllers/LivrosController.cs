﻿using System.Collections.Generic;
using AutoMapper;
using Livraria.Application.Interfaces;
using Livraria.Application.Models;
using Livraria.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Livraria.Site.Controllers
{
    public class LivrosController : Controller
    {
        private readonly ILivroAppService _livroAppService;
        private readonly IMapper _mapper;

        public LivrosController(ILivroAppService livroAppService, IMapper mapper)
        {
            _livroAppService = livroAppService;
            _mapper = mapper;
        }

        // GET: Livros
        public ActionResult Index()
        {
            var result = _livroAppService.GetAll();
            var model = result.Count > 0 ? _mapper.Map<List<LivroViewModel>>(result) : new List<LivroViewModel>();
            return View(model);
        }

        // GET: Livros/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Livros/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Livros/Create
        [HttpPost]
        public ActionResult Create(LivroViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _livroAppService.Insert(_mapper.Map<Livro>(model));
                    if (!result.IsValid())
                    {
                        foreach (var item in result.MessagesValidations())
                            ModelState.AddModelError("", item);
                        return View(model);
                    }
                    return RedirectToAction(nameof(Index));
                }


                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Livros/Edit/5
        public ActionResult Edit(int id)
        {
            if (ModelState.IsValid && id > 0)
            {
                var result = _livroAppService.GetById(id);
                if (result.Id > 0)
                    return View(_mapper.Map<LivroViewModel>(result));

            }
            return RedirectToAction(nameof(Index));
        }

        // POST: Livros/Edit/5
        [HttpPost]
        public ActionResult Edit(LivroViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _livroAppService.Update(_mapper.Map<Livro>(model));
                    if (result.Id == 0)
                        return View(model);
                    return RedirectToAction(nameof(Index));
                }


                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Livros/Delete/5
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid && id > 0)
            {
                var result = _livroAppService.GetById(id);
                if (result.Id > 0)
                    return View(_mapper.Map<LivroViewModel>(result));

            }
            return RedirectToAction(nameof(Index));
        }

        // POST: Livros/Delete/5
        [HttpPost]
        public ActionResult Delete(LivroViewModel model)
        {
            try
            {
                if (model.Id > 0)
                {
                    _livroAppService.Delete(model.Id);
                    return RedirectToAction(nameof(Index));
                }


                return View(model);
            }
            catch
            {
                return View(model);
            }
        }
    }
}