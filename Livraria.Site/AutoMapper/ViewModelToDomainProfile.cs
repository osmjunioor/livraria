﻿using AutoMapper;
using Livraria.Application.Models;
using Livraria.Domain.Entities;

namespace Livraria.Site.AutoMapper
{
    public class ViewModelToDomainProfile : Profile
    {
        public ViewModelToDomainProfile()
        {
            CreateMap<LivroViewModel, Livro>();
        }
    }
}
